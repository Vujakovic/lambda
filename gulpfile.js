'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sassLint = require('gulp-sass-lint');
var consolidate = require('gulp-consolidate');
var iconfont = require('gulp-iconfont');


// SCSS COMPILE
gulp.task('sass', function () {
  return gulp.src('./scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css/'));
});



//SASS LINT
gulp.task('sasslint', function () {
  return gulp.src('./scss/**/*.scss')
    .pipe(sassLint())
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError())
});


//WATCH
gulp.task('watch', function () {
  gulp.watch('./scss/**/*.scss', gulp.series(['sass', 'sasslint']))
});




//ICONFONT
gulp.task('iconfonts', function () {
  return gulp.src('./iconfont-src/svg/*.svg')
    .pipe(iconfont({
      fontName: 'iconfont',
      formats: ['ttf', 'eot', 'woff', 'woff2'],
      appendCodepoints: true,
      appendUnicode: false,
      normalize: true,
      fontHeight: 1000,
      centerHorizontally: true
    }))
    .on('glyphs', function (glyphs, options) {
      gulp.src('./iconfont-src/_iconfont.scss')
        .pipe(consolidate('underscore', {
          glyphs: glyphs,
          fontName: options.fontName,
          fontDate: new Date().getTime(),
          fontPath: "./fonts/"
        }))
        .pipe(gulp.dest('./scss/base/'));
    })
    .pipe(gulp.dest('./fonts/'));
});



//DEFAULT
gulp.task('default', gulp.series(['sass', 'sasslint', 'watch']));