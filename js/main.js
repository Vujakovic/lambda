$(document).ready(function () {

var galleryImages = ['item1', 'item2', 'item3', 'item4','item5', 'item6', 'item7', 'item8', 'item9', 'item10'];

galleryImages.forEach(img => {

    $(".gallery-items").append('<div class="gallery-item"><figure class="gallery-img" style="background-image:url(../assets/img/'+img+'.png)"></figure></div>');
});


var special = '<span class="special-tag">Special</span>';
$(".special").append(special);

$(".gotomenu").click(function (){
    $('html, body').animate({
        scrollTop: ($(".menu").offset().top)
    }, 1000);
});

$(".bookatable").click(function (){
    $('html, body').animate({
        scrollTop: ($(".reservations").offset().top)
    }, 1000);
});


$(".header__hamburger").click(function () {
    
    $(this).toggleClass("show-btn");
    $(".navigation").toggleClass("show-menu");
    $(".navigation").toggleClass("swing-in-top-fwd");
    });

$(".menu-item").click(function() {
    $(this).find(".meals").toggleClass("show-meals").toggleClass("swing-in-top-fwd");
    $(this).find(".arrow").toggleClass("down");
    $(this).find(".arrow").toggleClass("up");
});



$(".gallery-item").click(function() {
    $(".modal-content").html($(this).html());
    $(".modal").toggleClass("show-modal");
});

$(".x-button").click(function() {
    $(".modal").toggleClass("show-modal");
});



});