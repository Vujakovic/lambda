"use strict";

$(document).ready(function () {
  var special = '<span class="special-tag">Special</span>';
  $(".special").append(special);
  $(".gotomenu").click(function () {
    $('html, body').animate({
      scrollTop: $(".menu").offset().top
    }, 1000);
  });
  $(".bookatable").click(function () {
    $('html, body').animate({
      scrollTop: $(".reservations").offset().top
    }, 1000);
  });
});